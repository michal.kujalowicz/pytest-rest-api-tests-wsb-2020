import pytest
import requests
import json
from common import config


def create_issue(project, summary, description, issue_type):

    return create_issue_by_user(project, summary, description, issue_type, config.JIRA_USER, config.JIRA_PASSWORD)


def create_issue_by_user(project, summary, description, issue_type, user, password):

    project_id = get_project_id_with_user(project, user, password)
    issue_type_id = get_issue_type_id_with_user(issue_type, user, password)
    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "summary": summary,
            "description": description,
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(config.ISSUE_REST_URL, data, auth=(user, password),
                             headers={'content-type': 'application/json'})
    return response


def delete_issue(issue_key):
    response = requests.delete(config.ISSUE_REST_URL + "/" + issue_key, auth=(config.JIRA_USER, config.JIRA_PASSWORD), headers={'content-type': 'application/json'})
    return response


def get_project_id(project):
    project_id = get_project_id_with_user(project, config.JIRA_USER, config.JIRA_PASSWORD)
    return project_id


def get_project_id_with_user(project, user, password):
    response = requests.get(config.PROJECT_REST_URL + "/" + project, auth=(user, password))
    response_body = response.json()
    project_id = response_body["id"]
    return project_id


def get_issue_type_id(issue_type):
    issue_type_id = get_issue_type_id_with_user(issue_type, config.JIRA_USER, config.JIRA_PASSWORD)
    return issue_type_id


def get_issue_type_id_with_user(issue_type, username, password):
    response = requests.get(config.ISSUE_TYPE_REST_URL, auth=(username, password))
    response_body = response.json()
    default_id = 10004
    issue_type_id = default_id
    for issue_type_i in response_body:
        if issue_type_i["name"] == issue_type:
            issue_type_id = issue_type_i["id"]
    return issue_type_id


def create_user(username, password):

    email = username + "@example.com"
    data = json.dumps({
        "name": username,
        "password": password,
        "emailAddress": email,
        "displayName": username,
        "applicationKeys": [
            "jira-software"
        ]
    })
    response = requests.post(config.USER_REST_URL, data, auth=(config.JIRA_ADMIN_USER, config.JIRA_ADMIN_PASSWORD),
                             headers={'content-type': 'application/json'})
    return response


def create_project(projectkey, lead):

    data = json.dumps({
        "key": projectkey,
        "name": projectkey,
        "projectTypeKey": "software",
        "projectTemplateKey": "com.pyxis.greenhopper.jira:basic-software-development-template",
        "description": projectkey,
        "lead": lead
    })
    response = requests.post(config.PROJECT_REST_URL, data, auth=(config.JIRA_ADMIN_USER, config.JIRA_ADMIN_PASSWORD),
                             headers={'content-type': 'application/json'})
    return response


def get_project(project):

    url = config.PROJECT_REST_URL + "/" + project
    response = requests.get(url, auth=(config.JIRA_ADMIN_USER, config.JIRA_ADMIN_PASSWORD))
    return response


def get_project_role_id(url):
    response = requests.get(url, auth=(config.JIRA_ADMIN_USER, config.JIRA_ADMIN_PASSWORD))
    response_body = response.json()
    return response_body["id"]


def assign_user_to_role(projectkey, user, roleid):

    url = config.PROJECT_REST_URL + "/" + projectkey + "/role/" + str(roleid)
    data = json.dumps({
        "user": [user]
    })
    response = requests.post(url, data, auth=(config.JIRA_ADMIN_USER, config.JIRA_ADMIN_PASSWORD),
                             headers={'content-type': 'application/json'})
    return response