"""
Configuration
"""
from common import credentials


JIRA_TEST_INSTANCE_URL = "http://" + credentials.IP + ":8080"
PROJECT_REST_URL = JIRA_TEST_INSTANCE_URL + "/rest/api/2/project"
ISSUE_REST_URL = JIRA_TEST_INSTANCE_URL + "/rest/api/2/issue"
ISSUE_TYPE_REST_URL = JIRA_TEST_INSTANCE_URL + "/rest/api/2/issuetype"
USER_REST_URL = JIRA_TEST_INSTANCE_URL + "/rest/api/2/user"

JIRA_USER = credentials.JIRA_USER
JIRA_PASSWORD = credentials.JIRA_PASSWORD
JIRA_ADMIN_USER = credentials.JIRA_ADMIN_USER
JIRA_ADMIN_PASSWORD = credentials.JIRA_ADMIN_PASSWORD

PROJECT_NO_PERMISSION = credentials.JIRA_ADMIN_PROJECT
PROJECT_NO_PERMISSION_ID = credentials.JIRA_ADMIN_PROJECT_ID
PROJECT_WRITE_PERMISSION = credentials.JIRA_PROJECT

PROJECT_NOT_FOUND_ERROR_MESSAGE = "No project could be found with key '"+ PROJECT_NO_PERMISSION + "'."

