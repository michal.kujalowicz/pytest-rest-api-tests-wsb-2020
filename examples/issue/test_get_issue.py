import pytest
import requests
from common import config


def test_get_issue_successful(test_issue):
    """requesting for issue which exists and I have permission to"""
    response = requests.get(config.ISSUE_REST_URL + "/" + test_issue, auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 200


def test_get_issue_unauthorised(test_issue):
    """requesting for issue which exists unauthorised"""
    response = requests.get(config.ISSUE_REST_URL + "/" + test_issue)
    assert response.status_code == 401


def test_get_issue_no_permission():
    """requesting for issue to which I do not have permission to"""
    response = requests.get(config.ISSUE_REST_URL + "/" + config.PROJECT_NO_PERMISSION + "-1",
                            auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 403


def test_get_issue_issue_not_existing():
    """requesting for issue which does not exist"""
    response = requests.get(config.ISSUE_REST_URL + "/" + config.PROJECT_WRITE_PERMISSION + "-999999",
                            auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 404

