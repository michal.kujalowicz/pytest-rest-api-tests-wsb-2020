import pytest
import requests
import json
from common import config
from common import helpers


def test_delete_issue_successful(test_issue_to_delete):
    """deleting issue I have permission to"""
    response = requests.delete(config.ISSUE_REST_URL + "/" + test_issue_to_delete, auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 204
    assert response.text == ''
