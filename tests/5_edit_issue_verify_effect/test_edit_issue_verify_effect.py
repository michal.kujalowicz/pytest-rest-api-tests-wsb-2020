import pytest
import requests
import json
from common import config
from common import helpers


def test_edit_issue_edit_summary(test_issue):
    """editing summary of issue I have permission to"""
    summary = "New summary"
    data = json.dumps({
        "fields": {
            "summary": summary
        }
    })
    response = requests.put(config.ISSUE_REST_URL + "/" + test_issue, data, auth=(config.JIRA_USER, config.JIRA_PASSWORD),
                            headers={'content-type': 'application/json'})
    assert response.status_code == 204
    assert response.text == ''
    """ TODO TASK1 Wykonaj dodatkowy request i sprawdź czy rzeczywiście summary zostało zmienione"""
