import pytest
import requests
import json
from common import config
from common import helpers

"""TODO TASK1 Przejdź przez test i upewnij się że rozumiesz każdą linię"""


def test_create_issue():
    """creating issue in project I have permission to"""
    project_id = helpers.get_project_id(config.PROJECT_WRITE_PERMISSION)
    issue_type_id = helpers.get_issue_type_id("Bug")
    data = json.dumps({
        "fields": {
            "project": {
                "id": project_id
            },
            "summary": "This is a test issue",
            "description": "This is a test issue",
            "issuetype": {
                "id": issue_type_id
            }
        }
    })
    response = requests.post(config.ISSUE_REST_URL, data, auth=(config.JIRA_USER, config.JIRA_PASSWORD),
                             headers={'content-type': 'application/json'})
    assert response.status_code == 201
    response_body = response.json()

    """ Cleanup """
    issue_key = response_body['key']
    helpers.delete_issue(issue_key)

