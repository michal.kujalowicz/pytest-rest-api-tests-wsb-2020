import pytest
import requests
from common import config

PROJECT_REST_URL = config.JIRA_TEST_INSTANCE_URL + "/rest/api/2/project"


def test_get_project_authorised_successful():
    """requesting for specific project when authorised and 1 project is returned"""
    project_url = PROJECT_REST_URL + "/" + config.PROJECT_WRITE_PERMISSION
    response = requests.get(project_url, auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 200
    response_body = response.json()
    """ TODO TASK1 Dodaj asercję sprawdzającą czy w zwróconym JSON jest klucz projektu"""
    """ TODO TASK2 Dodaj asercję sprawdzającą czy Twój użytkownik jest lead'em projektu"""


def test_get_project_no_permission():
    """requesting for specific project and no permission"""
    project_url = PROJECT_REST_URL + "/" + config.PROJECT_NO_PERMISSION
    response = requests.get(project_url, auth=(config.JIRA_USER, config.JIRA_PASSWORD))
    assert response.status_code == 404
    response_body = response.json()
    """ TODO TASK 3 Dodaj asercję sprawdzającą tekst błędu"""
